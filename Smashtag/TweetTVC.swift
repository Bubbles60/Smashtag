//
//  TweetTVC.swift
//  Smashtag
//
//  Created by DFINLAY-AIR on 13/04/16.
//  Copyright © 2016 dcf.matcom. All rights reserved.
//

import UIKit

class TweetTVC: UITableViewController,UITextFieldDelegate {
    var tweets = [[Tweet]]()
    var searchText: String? = "#stanford"
        {
        didSet {
            tweets.removeAll()
            tableView.reloadData()
            refresh()
        }
    }
    @IBOutlet weak var searchTextField: UITextField!{
        didSet{
            searchTextField?.text = searchText
             searchTextField.delegate = self
            searchTextField.text = searchText
            refresh()
        }
       
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == searchTextField {
            resignFirstResponder()
            searchText = textField.text
        }
        return true
    }
    func refresh(){
        if searchText != nil {
            let request = TwitterRequest(search: searchText!, count: 100)
            request.fetchTweets { (newTweets) -> Void in
                // note we use trailing closure last argument can be passed after function
                dispatch_async(dispatch_get_main_queue()) { () -> Void in
                    if newTweets.count > 0
                    {
                        self.tweets.insert(newTweets, atIndex: 0)
                        self.tableView.reloadData()
                    }
                }
                
            }
        }
    }
    
    // MARK: -
    
    override func viewDidLoad() {
       refresh()
    }
    
    // MARK: - Tableview data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return tweets.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweets[section].count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Tweet", forIndexPath: indexPath) as UITableViewCell
        // Configure the cell
        let tweet = tweets[indexPath.section][indexPath.row]
        cell.textLabel?.text = tweet.text
        
        return cell
        
    }

}
